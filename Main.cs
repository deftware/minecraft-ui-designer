﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Minecraft_UI_Designer.Controls;
using Minecraft_UI_Designer.Parser;
using Newtonsoft.Json.Linq;

namespace Minecraft_UI_Designer
{
    public partial class Main : Form
    {

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private PrivateFontCollection fonts = new PrivateFontCollection();

        private List<UserControl> MinecraftControls = new List<UserControl> {
            new MinecrafTextbox(),
            new MinecraftButton(),
            new MinecraftText()
        };

        private ControlListBox controlListBox;

        public static Font minecraftFont;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SetLocationAndSize();
            controlListBox = new ControlListBox();
            splitContainer1.Panel2.Controls.Add(controlListBox);
            controlListBox.Dock = DockStyle.Fill;
            controlListBox.MouseDoubleClick += controlListBox_MouseDoubleClick;
            MinecraftControls.ForEach(Control => {
                controlListBox.Items.Add(Control);
            });
            // https://stackoverflow.com/questions/556147/how-to-quickly-and-easily-embed-fonts-in-winforms-app-in-c-sharp
            byte[] fontData = Properties.Resources.minecraft;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.minecraft.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.minecraft.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            minecraftFont = new Font(fonts.Families[0], 12.0F);
        }

        private void Main_Resize(object sender, EventArgs e)
        {
            SetLocationAndSize();
        }

        private void SetLocationAndSize()
        {
            this.MinimumSize = new Size(panel2.Width + splitContainer1.Panel2.Width + splitContainer1.SplitterWidth, panel2.Height + (menuStrip1.Height * 3));
            panel2.Location = new Point((splitContainer1.Panel1.Width / 2) - (panel2.Width / 2), (splitContainer1.Panel1.Height / 2) - (panel2.Height / 2));
        }

        private void controlListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            UserControl userControl = (UserControl) controlListBox.SelectedItem;
            UserControl newInstance = (UserControl) Activator.CreateInstance(userControl.GetType());
            panel1.Controls.Add(newInstance);
            newInstance.Location = new Point(40, 40);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Control> controls = new List<Control>();
            foreach (Control c in panel1.Controls)
            {
                controls.Add(c);
            }
            LayoutToJsonParser parser = new LayoutToJsonParser(controls);
            JObject data = parser.parse();
            Console.WriteLine(data.ToString());

            JsonToJavaParser p2 = new JsonToJavaParser("me.deftware.client.framework.TestUI");
            p2.ParseJson(data);
            Console.WriteLine(p2.Compile());
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void javaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
