﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Minecraft_UI_Designer.Controls;

namespace Minecraft_UI_Designer
{
    public class ControlListBox : ListBox
    {

        public ControlListBox()
        {
            this.DrawMode = DrawMode.OwnerDrawFixed;
            this.ItemHeight = 45;
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            const TextFormatFlags flags = TextFormatFlags.Left | TextFormatFlags.VerticalCenter;
            
            if (e.Index >= 0 && Items[e.Index] is IMinecraftControl)
            {
                var control = (IMinecraftControl) Items[e.Index];
                e.DrawBackground();
                var textRect = e.Bounds;
                textRect.X += 20;
                textRect.Width -= 20;
                if (control.GetControlImage() != null)
                {
                    e.Graphics.DrawImage(control.GetControlImage(), 2, e.Bounds.Y + 2, this.Width, 40);
                    TextRenderer.DrawText(e.Graphics, control.GetControlName(), Main.minecraftFont, textRect, Color.White, flags);
                } else
                {
                    TextRenderer.DrawText(e.Graphics, control.GetControlName(), Main.minecraftFont, textRect, Color.Black, flags);
                }
                e.DrawFocusRectangle();
            }
        }

    }
}
