﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minecraft_UI_Designer.Controls
{
    public partial class MinecraftButton : DraggableUserControl, IMinecraftControl
    {
        public MinecraftButton()
        {
            InitializeComponent();
            AddHandlers(editableLabel1.GetMoveControl());
        }

        public string GetControlData()
        {
            return editableLabel1.GetText();
        }

        public Image GetControlImage()
        {
            return Properties.Resources.button;
        }

        public string GetControlName()
        {
            return "Button";
        }

        public string GetJavaConstructor(int X, int Y, int Width, int Height)
        {
            return string.Format("IGuiButton({4}, {0}, {1}, {2}, {3}, \"{5}\")", X, Y, Width, Height, X + Y, editableLabel1.GetText());
        }

        public string GetJavaControlName()
        {
            return "me.deftware.client.framework.wrappers.gui.IGuiButton";
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public Size GetSize()
        {
            return this.Size;
        }

        private void MinecraftButton_Load(object sender, EventArgs e)
        {

        }
    }
}
