﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_UI_Designer.Controls
{
    interface IMinecraftControl
    {

        string GetJavaControlName();

        string GetJavaConstructor(int X, int Y, int Width, int Height);

        string GetControlName();

        string GetControlData();

        Image GetControlImage();

        Point GetLocation();

        Size GetSize();

    }
}
