﻿namespace Minecraft_UI_Designer.Controls
{
    partial class MinecrafTextbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MinecrafTextbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Minecraft_UI_Designer.Properties.Resources.textbox;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Name = "MinecrafTextbox";
            this.Size = new System.Drawing.Size(299, 44);
            this.Load += new System.EventHandler(this.MinecrafTextbox_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MinecrafTextbox_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MinecrafTextbox_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MinecrafTextbox_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
