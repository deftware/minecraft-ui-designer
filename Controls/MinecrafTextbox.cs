﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minecraft_UI_Designer.Controls
{
    public partial class MinecrafTextbox : DraggableUserControl, IMinecraftControl
    {

        public MinecrafTextbox()
        {
            InitializeComponent();
        }

        public string GetControlName()
        {
            return "Textbox";
        }

        public string GetJavaConstructor(int X, int Y, int Width, int Height)
        {
            return string.Format("IGuiTextField({4}, {0}, {1}, {2}, {3})", X, Y, Width, Height, X + Y);
        }

        public string GetJavaControlName()
        {
            return "me.deftware.client.framework.wrappers.gui.IGuiTextField";
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public Size GetSize()
        {
            return this.Size;
        }

        private void MinecrafTextbox_Load(object sender, EventArgs e)
        {

        }

        private void MinecrafTextbox_MouseDown(object sender, MouseEventArgs e)
        {
          
        }

        private void MinecrafTextbox_MouseUp(object sender, MouseEventArgs e)
        {
     
        }

        private void MinecrafTextbox_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        public Image GetControlImage()
        {
            return Properties.Resources.textbox;
        }

        public string GetControlData()
        {
            return "";
        }
    }
}
