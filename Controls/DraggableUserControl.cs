﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minecraft_UI_Designer.Controls
{
    public class DraggableUserControl : UserControl
    {

        private Point MouseDownLocation;

        public DraggableUserControl()
        {
            this.MouseDown += MouseDownEventHandler;
            this.MouseMove += MouseMoveEventHandler;
        }

        protected void AddHandlers(Control Control)
        {
            Control.MouseDown += MouseDownEventHandler;
            Control.MouseMove += MouseMoveEventHandler;
        }

        private void MouseDownEventHandler(object sender, MouseEventArgs e)
        {
            MouseDownLocation = e.Location;
        }

 
        private void MouseMoveEventHandler(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

    }
}
