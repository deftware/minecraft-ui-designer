﻿namespace Minecraft_UI_Designer.Controls
{
    partial class MinecraftButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editableLabel1 = new Minecraft_UI_Designer.Controls.EditableLabel();
            this.SuspendLayout();
            // 
            // editableLabel1
            // 
            this.editableLabel1.BackColor = System.Drawing.Color.Transparent;
            this.editableLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editableLabel1.Location = new System.Drawing.Point(0, 0);
            this.editableLabel1.Name = "editableLabel1";
            this.editableLabel1.Size = new System.Drawing.Size(210, 40);
            this.editableLabel1.TabIndex = 0;
            // 
            // MinecraftButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Minecraft_UI_Designer.Properties.Resources.button;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.editableLabel1);
            this.Name = "MinecraftButton";
            this.Size = new System.Drawing.Size(210, 40);
            this.Load += new System.EventHandler(this.MinecraftButton_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private EditableLabel editableLabel1;
    }
}
