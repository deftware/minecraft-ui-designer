﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Text;

namespace Minecraft_UI_Designer.Controls
{
    public partial class EditableLabel : UserControl
    {

        PrivateFontCollection pfc = new PrivateFontCollection();

        public EditableLabel()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            textBox1.Visible = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Visible = false;
                textBox1.Text = textBox1.Text.Trim();
            }
        }

        public string GetText()
        {
            return label1.Text;
        }

        public Control GetMoveControl()
        {
            return label1;
        }

        private void EditableLabel_Load(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            label1.Font = Main.minecraftFont;
            textBox1.Font = Main.minecraftFont;
        }
    }
}
