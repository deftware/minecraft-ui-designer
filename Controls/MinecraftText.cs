﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minecraft_UI_Designer.Controls
{
    public partial class MinecraftText : DraggableUserControl, IMinecraftControl
    {
        public MinecraftText()
        {
            InitializeComponent();
            AddHandlers(editableLabel1.GetMoveControl());
        }

        public string GetControlData()
        {
            return editableLabel1.GetText();
        }

        public Image GetControlImage()
        {
            return null;
        }

        public string GetControlName()
        {
            return "Text Label";
        }

        public string GetJavaConstructor(int X, int Y, int Width, int Height)
        {
            return "";
        }

        public string GetJavaControlName()
        {
            return "IFontRenderer";
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public Size GetSize()
        {
            return this.Size;
        }
    }
}
