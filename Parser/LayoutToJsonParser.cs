﻿using Minecraft_UI_Designer.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minecraft_UI_Designer.Parser
{
    public class LayoutToJsonParser
    {

        private List<IMinecraftControl> Controls = new List<IMinecraftControl>();

        public LayoutToJsonParser(List<Control> Controls)
        {
            Controls.ForEach(c => this.Controls.Add((IMinecraftControl) c));
        }

        public JObject parse()
        {
            JObject json = new JObject();
            JArray controlArr = new JArray();
            Controls.ForEach(c =>
            {
                JObject controlJson = new JObject();
                controlJson["ControlName"] = c.GetControlName();
                controlJson["Constructor"] = c.GetJavaConstructor(ScaleIntToMinecraft(c.GetLocation().X), ScaleIntToMinecraft(c.GetLocation().Y), ScaleIntToMinecraft(c.GetSize().Width), ScaleIntToMinecraft(c.GetSize().Height));
                controlJson["Package"] = c.GetJavaControlName();
                controlJson["Data"] = c.GetControlData();
                JObject location = new JObject(), size = new JObject();
                location["X"] = c.GetLocation().X;
                location["Y"] = c.GetLocation().Y;
                size["Width"] = c.GetSize().Width;
                size["Height"] = c.GetSize().Height;
                controlJson["Location"] = location;
                controlJson["Size"] = size;
                controlArr.Add(controlJson);
            });
            json["Controls"] = controlArr;
            return json;
        }

        public int ScaleIntToMinecraft(int OriginalInt)
        {
            return OriginalInt / 2;
        }

    }

}
