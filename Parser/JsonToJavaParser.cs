﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

/**
 * Made by Deftware
 * Converts a Json object to java code
 */
namespace Minecraft_UI_Designer.Parser
{
    public class JsonToJavaParser
    {

        private readonly string QualifiedClassName;
        public string MethodAccessibility = "protected", ClassAccessibility = "public", VariableAccessibility = "private";

        private readonly List<string> MethodNames = new List<string>
        {
            "onInitGui()",
            "onDraw(int mouseX, int mouseY, float partialTicks)",
            "onUpdate()",
            "onKeyPressed(int keyCode, int action, int modifiers)",
            "onMouseReleased(int mouseX, int mouseY, int mouseButton)",
            "onMouseClicked(int mouseX, int mouseY, int mouseButton)",
            "onGuiResize(int w, int h)"
        };

        private List<string> Imports = new List<string>
        {
            "me.deftware.client.framework.wrappers.gui.IGuiScreen"
        };

        private List<string> Variables = new List<string>();

        private List<KeyValuePair<string, List<String>>> Methods = new List<KeyValuePair<string, List<String>>>();

        public void AddToMethod(string Method, string Data)
        {
            Methods.ForEach(m =>
            {
                if (m.Key.Equals(Method) || m.Key.StartsWith(Method))
                {
                    m.Value.Add(Data);
                }
            });
        }

        public void PopulateBoilerPlate()
        {
            MethodNames.ForEach(m => Methods.Add(new KeyValuePair<string, List<String>>(m, new List<string>())));
            AddToMethod("onDraw", "this.drawIDefaultBackground();");
            AddToMethod("onInitGui", "this.clearButtons();");
        }

        public JsonToJavaParser(string QualifiedClassName)
        {
            this.QualifiedClassName = QualifiedClassName;
            PopulateBoilerPlate();
        }

        public void AddOrUpdateVariable(string VariableType, string VariableName)
        {
            string ReplaceVar = Variables.FirstOrDefault(x => x.StartsWith(VariableType));
            if (ReplaceVar != null)
            {
                Variables.Remove(ReplaceVar);
                Variables.Add(string.Format("{0}, {1}", ReplaceVar, VariableName));
            } else
            {
                Variables.Add(string.Format("{0} {1}", VariableType, VariableName));
            }
        }

        public void ParseJson(JObject Json)
        {
            int id = 0;
            foreach (JObject Control in Json["Controls"])
            {
                if (Imports.FirstOrDefault(x => x == Control["Package"].Value<string>()) == null)
                {
                    Imports.Add(Control["Package"].Value<string>());
                }
                string ControlClassName = Control["Package"].Value<string>();
                ControlClassName = ControlClassName.Split('.')[ControlClassName.Split('.').Length - 1];
                switch (Control["ControlName"].Value<string>())
                {
                    case "Button":
                        AddToMethod("onInitGui", string.Format("this.addButton(new {0} {1}", Control["Constructor"].Value<string>(), "{"));
                        AddToMethod("onInitGui", "\t@Override\n\t\t\tpublic void onButtonClick(double mouseX, double mouseY) {\n\n\t\t\t}\n\t\t});");
                        break;
                    case "Textbox":
                        AddOrUpdateVariable(ControlClassName, "textBox" + id);
                        AddToMethod("onInitGui", string.Format("this.{0} = new {1};", "textBox" + id, Control["Constructor"].Value<string>()));
                        AddToMethod("onInitGui", string.Format("this.addEventListener({0});", "textBox" + id));
                        AddToMethod("onDraw", string.Format("this.{0}.onDraw(mouseX, mouseY, partialTicks);", "textBox" + id));
                        AddToMethod("onUpdate", string.Format("this.{0}.doCursorTick();", "textBox" + id));
                        AddToMethod("onKeyPressed", string.Format("this.{0}.onKeyPressed(keyCode, action, modifiers);", "textBox" + id));
                        AddToMethod("onMouseClicked", string.Format("this.{0}.onMouseClicked(mouseX, mouseY, mouseButton);", "textBox" + id));
                        break;
                }
                id++;
            }
        }

        // 427 x 240

        public string Compile()
        {
            string ClassName = QualifiedClassName.Split('.')[QualifiedClassName.Split('.').Length - 1], Package = QualifiedClassName.Substring(0, QualifiedClassName.Length - ClassName.Length - 1);
            List<string> Data = new List<string>
            {
                string.Format("package {0};\n", Package)
            };
            Imports.ForEach(Import =>
            {
                Data.Add(string.Format("import {0};", Import));
            });
            Data.Add(string.Format("\n{0} class {1} extends IGuiScreen {2}\n", ClassAccessibility, ClassName, "{"));
            Variables.ForEach(Variable =>
            {
                Data.Add(string.Format("\t{0} {1};", VariableAccessibility, Variable));
            });
            Methods.ForEach(Method =>
            {
                Data.Add(string.Format("\n\t@Override\n\t{0} void {1} {2} {3}", MethodAccessibility, Method.Key, "{", Method.Value.Count == 0 ? "\n" : ""));
                Method.Value.ForEach(Line =>
                {
                    Data.Add(string.Format("\t\t{0}", Line));
                });
                Data.Add("\t}");
            });
            Data.Add("\n}\n");
            return string.Join("\n", Data.ToArray());
        }

    }
}
